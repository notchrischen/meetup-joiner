/* TypeScript file generated from start.res by genType. */
/* eslint-disable import/first */


// @ts-ignore: Implicit any on import
import * as Curry__Es6Import from 'rescript/lib/es6/curry.js';
const Curry: any = Curry__Es6Import;

// @ts-ignore: Implicit any on import
import * as startBS__Es6Import from './start.mjs';
const startBS: any = startBS__Es6Import;

import type {Page_t as Playwright_Page_t} from './Playwright.gen';

import type {State_t as Persistence_State_t} from './Persistence.gen';

// tslint:disable-next-line:interface-over-type-literal
export type Event_t = string;

// tslint:disable-next-line:interface-over-type-literal
export type LoginPage_credentials = { readonly email: string; readonly password: string };

// tslint:disable-next-line:interface-over-type-literal
export type GroupPage_t = [Playwright_Page_t, string];

export const GroupPage_make: (_1:Playwright_Page_t, _2:string) => Promise<GroupPage_t> = function (Arg1: any, Arg2: any) {
  const result = Curry._2(startBS.GroupPage.make, Arg1, Arg2);
  return result
};

export const GroupPage_getEvents: (_1:GroupPage_t) => Promise<Event_t[]> = startBS.GroupPage.getEvents;

export const tryJoinEvents: (page:Playwright_Page_t, events:Event_t[], state:Persistence_State_t) => Array<() => Promise<Event_t>> = function (Arg1: any, Arg2: any, Arg3: any) {
  const result = Curry._3(startBS.tryJoinEvents, Arg1, Arg2, Arg3);
  return result
};

export const doLogin: (page:Playwright_Page_t, credentials:LoginPage_credentials) => Promise<void> = function (Arg1: any, Arg2: any) {
  const result = Curry._2(startBS.doLogin, Arg1, Arg2);
  return result
};
