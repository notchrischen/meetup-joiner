import * as dotenv from 'dotenv'
import { joinEvents } from './helpers.js'

dotenv.config()

const eventSlug = process.argv[2]
console.log(eventSlug)

await (async function () {
  while (true) {
    const { page } = await joinEvents(eventSlug)
    await page.waitForTimeout(2 * 60 * 1000)
  }
})()
