module FileString: {
  type t = string
  @module("fs/promises")
  external readFile: (t, string) => Js.Promise.t<string> = "readFile"
  @module("fs/promises")
  external writeFile: (t, string) => Js.Promise.t<unit> = "writeFile"
} = {
  type t = string
  @module("fs/promises")
  external readFile: (t, string) => Js.Promise.t<string> = "readFile"
  @module("fs/promises")
  external writeFile: (t, string) => Js.Promise.t<unit> = "writeFile"
}

module Set = Js_set

module JSON: {
  type t
} = {
  type t = string
}

module SerializedState = {
  type t = array<string>
  @scope("JSON") @val
  external fromString: string => t = "parse"

  let toString = Js.Json.serializeExn
}

module ArrayExt = {
  open Array
  type t<'a> = array<'a>
  let tap = (arr, cb) => {
    arr->map(cb)->ignore
  }
  @scope("Array") @val
  external fromStringSet: Set.t<string> => array<string> = "from"
}

module State: {
  @genType
  type t
  @genType
  let empty: unit => t
  let has: (t, string) => bool
  let notHave: (t, string) => bool
  let hasOpt: (t, string) => option<string>
  @genType
  let add: (t, string) => t
  @genType
  let addArray: (t, array<string>) => t
  let deserialize: string => t
  let serialize: t => string
} = {
  type t = Set.t<string>
  let has = (t, e: string) => Set.has(t, (e :> string))
  let notHave = (t, e: string) => !(t->Set.has((e :> string)))
  let hasOpt = (t, entry: string) =>
    switch t->has(entry) {
    | true => Some(entry)
    | false => None
    }
  let empty = () => Set.make()
  let add = (t, e: string) => Set.add(t, (e :> string))
  let addArray = (set, arr: array<string>) => {
    arr->Belt.Array.forEach(e => set->add(e)->ignore)->ignore
    set
  }

  let deserialize = str => str->SerializedState.fromString->Set.fromEntries

  let serialize = (t: t) => {
    t->ArrayExt.fromStringSet->SerializedState.toString
  }
}

module PersistedState: {
  @genType
  type t = {
    file: string,
    state: State.t,
  }
  @genType
  let load: string => Js.Promise.t<option<t>>
  @genType
  let save: t => Js.Promise.t<option<unit>>
} = {
  type t = {
    file: string,
    state: State.t,
  }
  let load = (file: string) => {
    try {
      file
      ->FileString.readFile("utf8")
      ->Promise.thenResolve(str => {
        Js.Console.log(str)
        switch str {
        | "" => None
        | _ => {
            let state = str->State.deserialize
            Some({file, state})
          }
        }
      })
    } catch {
    | _ => Promise.resolve(None)
    }
  }

  let save = state => {
    try {
      state.state
      ->State.serialize
      ->FileString.writeFile(state.file, _)
      ->Promise.thenResolve(x => x->Some)
    } catch {
    | _ => Promise.resolve(None)
    }
  }
}
