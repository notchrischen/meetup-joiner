import { webkit, devices } from 'playwright'
import type { Page, BrowserContext, Browser } from 'playwright'
import fs from "fs";
import type { State_t } from './Persistence.gen.js'
import { GroupPage_make, GroupPage_getEvents, tryJoinEvents, Event_t, doLogin } from './Meetup.gen.js'
import { Locator_click, Page_getOneTrustBtn } from './Playwright.gen.js'
import { PersistedState_load, PersistedState_save, State_empty, State_addArray } from './Persistence.gen.js'

/** Utility Functions **/
export const runSync = async <T>(jobs: Array<() => Promise<T>>): Promise<T[]> => {
  const results: T[] = []
  for (let i = 0; i < jobs.length; i++) {
    results.push(await (jobs[i]()))
  }

  return results
}

export type Maybe<T> = T | undefined

export const Maybe = {
  map: <A, B>(f: (a: A) => B) => (v: Maybe<A>): Maybe<B> => v !== undefined ? f(v) : undefined,
  fold: <A, B>(none: B, some: (a: A) => B, maybe: Maybe<A>) => {
    if (maybe === undefined) {
      return none
    } else {
      return some(maybe)
    }
  },
  flatMap: <A, B>(f: (a: A) => Maybe<B>, maybe: Maybe<A>): Maybe<B> => {
    if (maybe !== undefined) {
      return f(maybe)
    }
  },
  bimap: (f1, f2, maybe) => {
    return (maybe === undefined) ? f2() : f1(maybe)
  }
}

export interface Credential {
  email: string
  password: string
}

export const Credential = {
  make: (email: string) => (password: string): Credential => ({
    email,
    password
  })
}

export const joinEvents = async (eventSlug: string): Promise<{ page: Page, context: BrowserContext, browser: Browser }> => {
  const browser = await webkit.launch({ headless: true })
  const context = await browser.newContext({
    ...devices['Desktop Safari'],
    storageState: fs.existsSync('storageState.json') ? 'storageState.json' : undefined
  })
  const page = await context.newPage()

  const credential = Maybe.flatMap(
    email => Maybe.flatMap(
      password => Credential.make(email)(password),
      process.env.PASSWORD
    ),
    process.env.EMAIL)

  const cookies = await context.cookies()
  // const session = await context.storageState({ path: 'storageState.json' })
  if (cookies.findIndex(x => x.name === 'MEETUP_MEMBER') < 0) {
    console.log('Session not found. Doing login.')

    await Maybe.bimap(
      async (credential: Credential) => { await doLogin(page, credential) },
      () => { throw Error('.env file required.') },
      credential)

    await context.storageState({ path: 'storageState.json' })
  }

  const state = await PersistedState_load('./events.state')
  let joined: State_t = Maybe.fold(
    State_empty(),
    s => s.state, state
  )
  console.log('loaded state')
  console.log(joined)

  // Create a GroupPage
  // GroupPage is a datatype that encodes a Page + event slug
  const groupPage = await GroupPage_make(page, eventSlug)

  // Get events from the GroupPage
  const events: Event_t[] = await GroupPage_getEvents(groupPage)

  console.log(`Found ${events.length} events`)

  // Close the GDRP modal
  await Page_getOneTrustBtn(page)
    .then(Maybe.map(x => Locator_click(x, null)))

  // Try to join events and return an array of lazy promises that do that
  const actions: Array<Promise<string>> = tryJoinEvents(context, events, joined)

  console.log(`\t${actions.length} new event(s)`)

  // Run each join promise synchronously
  // const results = await runSync<string>(actions)
  // Run promises simultaneously and get the results of successful ones
  const results = (await Promise.allSettled(actions)).map((res: PromiseSettledResult<null | undefined | string>) => res.status === 'fulfilled' ? res.value : undefined).filter(x => x)

  // Update joined state
  // Native JS Set is mutable, but treating as immutable.
  // @TODO: Switch to immutable.js version of Set
  joined = State_addArray(joined, results)

  console.log('Saving')
  await PersistedState_save({ file: './events.state', state: joined })
  console.log('Saved')

  return { page, context, browser }
}

export const cleanupPlaywright = async ({ context, browser }): Promise<void> => {
  // Teardown
  await context.close()
  await browser.close()
}
