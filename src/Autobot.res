open Playwright
module Locator: {
  type t = Locator.t
  let get: (t, Locator.waitForOpts) => Js.Promise.t<option<Locator.t>>
  let tryClick: t => Js.Promise.t<option<Locator.t>>
  let tryType: (t, string, option<Playwright.Locator.typeOpts>) => Js.Promise.t<option<unit>>
} = {
  type t = Locator.t
  let get = (loc: t, opts) => {
    loc
    ->Locator.waitFor(opts)
    ->Promise.thenResolve(_ => {
      Js.log("Locator found...")
      Js.log(loc)
      Some(loc)
    })
    ->Promise.catch(e => {
      Js.log(e)
      Js.log("Locator not found")
      Js.log(loc)
      Promise.resolve(None)
    })
  }

  let tryClick = t => {
    t
    ->get(_, {state: #visible, timeout: 3000})
    ->Promise.then(
      Belt.Option.mapWithDefault(_, Promise.resolve(None), loc => {
        Js.log("\tClicking...")
        Js.log(loc)
        loc
        ->Locator.click(Some({timeout: 300}))
        ->Promise.thenResolve(_ => Some(loc))
        ->Promise.catch(_ => {
          Js.log("\tButton not found anymore. Skipping...")
          Promise.resolve(Some(loc))
        })
      }),
    )
  }

  let tryType = (t, text, opts) => {
    Playwright.Locator.type_(t, text, opts)
    ->Promise.thenResolve(r => Some(r))
    ->Promise.catch(_ => Js.Promise.resolve(None))
  }
}
