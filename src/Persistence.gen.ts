/* TypeScript file generated from Persistence.res by genType. */
/* eslint-disable import/first */


// @ts-ignore: Implicit any on import
import * as Curry__Es6Import from 'rescript/lib/es6/curry.js';
const Curry: any = Curry__Es6Import;

// @ts-ignore: Implicit any on import
import * as PersistenceBS__Es6Import from './Persistence.mjs';
const PersistenceBS: any = PersistenceBS__Es6Import;

// tslint:disable-next-line:max-classes-per-file 
export abstract class State_t { protected opaque!: any }; /* simulate opaque types */

// tslint:disable-next-line:interface-over-type-literal
export type PersistedState_t = { readonly file: string; readonly state: State_t };

export const State_empty: () => State_t = PersistenceBS.State.empty;

export const State_add: (_1:State_t, _2:string) => State_t = function (Arg1: any, Arg2: any) {
  const result = Curry._2(PersistenceBS.State.add, Arg1, Arg2);
  return result
};

export const State_addArray: (_1:State_t, _2:string[]) => State_t = function (Arg1: any, Arg2: any) {
  const result = Curry._2(PersistenceBS.State.addArray, Arg1, Arg2);
  return result
};

export const PersistedState_load: (_1:string) => Promise<(null | undefined | PersistedState_t)> = PersistenceBS.PersistedState.load;

export const PersistedState_save: (_1:PersistedState_t) => Promise<(null | undefined | void)> = PersistenceBS.PersistedState.save;

export const PersistedState: { load: (_1:string) => Promise<(null | undefined | PersistedState_t)>; save: (_1:PersistedState_t) => Promise<(null | undefined | void)> } = PersistenceBS.PersistedState
