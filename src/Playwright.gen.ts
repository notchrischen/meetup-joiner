/* TypeScript file generated from Playwright.res by genType. */
/* eslint-disable import/first */


// @ts-ignore: Implicit any on import
import * as Curry__Es6Import from 'rescript/lib/es6/curry.js';
const Curry: any = Curry__Es6Import;

// @ts-ignore: Implicit any on import
import * as PlaywrightBS__Es6Import from './Playwright.mjs';
const PlaywrightBS: any = PlaywrightBS__Es6Import;

import type {BrowserContext as $$BrowserContext_t} from 'playwright';

import type {Locator as $$Locator_t} from 'playwright';

import type {Page as $$Page_t} from 'playwright';

// tslint:disable-next-line:interface-over-type-literal
export type Locator_t = $$Locator_t;

// tslint:disable-next-line:interface-over-type-literal
export type Locator_clickOpts = { readonly timeout: number };

// tslint:disable-next-line:interface-over-type-literal
export type Page_t = $$Page_t;

// tslint:disable-next-line:interface-over-type-literal
export type BrowserContext_t = $$BrowserContext_t;

export const id: <T1>(id:T1) => T1 = PlaywrightBS.id;

export const Locator_click: (_1:Locator_t, _2:(null | undefined | Locator_clickOpts)) => Promise<void> = function (Arg1: any, Arg2: any) {
  const result = Curry._2(PlaywrightBS.Locator.click, Arg1, (Arg2 == null ? undefined : Arg2));
  return result
};

export const Page_getOneTrustBtn: (_1:Page_t) => Promise<(null | undefined | Locator_t)> = PlaywrightBS.Page.getOneTrustBtn;

export const Page: { getOneTrustBtn: (_1:Page_t) => Promise<(null | undefined | Locator_t)> } = PlaywrightBS.Page
