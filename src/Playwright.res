@genType
let id = id => id

module Node = {
  type t
}
module ElementHandle = {
  type t<'a>
  @send external getAttribute: (t<'a>, string) => Promise.t<option<string>> = "getAttribute"
  /* let getAttribute = (t, name) => t->_getAttribute(name) */
}
module Link: {
  type t = private string
  let make: ElementHandle.t<Node.t> => Promise.t<option<t>>
} = {
  open ElementHandle
  /* type t = ElementHandle.t<Node.t>; */
  type t = string
  let make = (eh: ElementHandle.t<Node.t>) => eh->getAttribute("href")
}

module Locator: {
  @genType.import(("playwright", "Locator"))
  type t

  type clickOpts = {timeout: int}
  @genType
  let click: (t, option<clickOpts>) => Js.Promise.t<unit>

  let isVisible: t => Js.Promise.t<bool>

  type states = [#attached | #detached | #visible | #hidden | #undefined]
  type waitForOpts = {
    state: states,
    timeout?: int,
  }
  let waitFor: (t, waitForOpts) => Js.Promise.t<unit>

  type locatorOpts = {
    has?: t,
    hasText?: string,
  }

  let locator: (t, string, option<locatorOpts>) => t

  let all: t => Js.Promise.t<array<t>>
  let nth: (t, int) => t
  let first: t => t
  let filter: (t, locatorOpts) => t

  type typeOpts = {
    delay?: int,
    noWaitAfter?: bool,
    timeout?: int,
  }
  let type_: (t, string, option<typeOpts>) => Js.Promise.t<unit>

  let toOption: t => Promise.t<option<t>>
} = {
  type t
  type states = [#attached | #detached | #visible | #hidden | #undefined]
  type clickOpts = {timeout: int}
  @send external click: (t, option<clickOpts>) => Js.Promise.t<unit> = "click"
  @send
  external isVisible: t => Promise.t<bool> = "isVisible"
  type waitForOpts = {
    state: states,
    timeout?: int,
  }
  @send
  external waitFor': (t, {"state": Js.Json.t, "timeout": option<int>}) => Promise.t<unit> =
    "waitFor"

  let waitFor = (t, waitForOpts) => {
    open JsonCombinators.Json.Encode
    t->waitFor'({
      "state": switch waitForOpts.state {
      | #attached => "attached" |> string
      | #detached => "detached" |> string
      | #visible => "visible" |> string
      | #hidden => "hidden" |> string
      | #undefined => null
      },
      "timeout": waitForOpts.timeout,
    })
  }

  type locatorOpts = {
    has?: t,
    hasText?: string,
  }
  @send external locator: (t, string, option<locatorOpts>) => t = "locator"
  @send external all: t => Js.Promise.t<Js.Array.t<t>> = "all"
  @send external nth: (t, int) => t = "nth"
  @send external first: t => t = "first"
  @send external filter: (t, locatorOpts) => t = "filter"
  type typeOpts = {
    delay?: int,
    noWaitAfter?: bool,
    timeout?: int,
  }
  @send external type_: (t, string, option<typeOpts>) => Js.Promise.t<unit> = "type"

  let toOption = t =>
    t
    ->isVisible
    ->Promise.thenResolve(b =>
      switch b {
      | true => Some(t)
      | false => None
      }
    )
  /* let map = (t: t, f) =>
    t
    ->isVisible
    ->Promise.then(b =>
      switch b {
      | true => f(t)
      | false => t
      }
    ) */
}

module Page: {
  @genType.import(("playwright", "Page"))
  type t
  let query: (t, string) => Promise.t<Js.Array.t<ElementHandle.t<Node.t>>>
  /* let getLinkElements: t => Promise.t<Js.Array.t<ElementHandle.t<Node.t>>>; */
  let getLinks: t => Js.Promise.t<Js.Array.t<Link.t>>

  let isVisible: (t, string) => Js.Promise.t<bool>
  let locator: (t, string) => Locator.t

  let close: t => Js.Promise.t<unit>

  @genType
  let getOneTrustBtn: t => Js.Promise.t<option<Locator.t>>
  let getMobileModal: t => Js.Promise.t<option<Locator.t>>

  let waitForTimeout: (t, int) => Js.Promise.t<unit>
  let waitForSelector: (t, string) => Js.Promise.t<ElementHandle.t<'a>>

  type navOpts = {
    timeout?: int,
    url?: string,
    waitUntil?: string,
  }
  let waitForNavigation: (~t: t, ~opts: navOpts=?, unit) => Promise.t<unit>

  type loadStateOpts = {timeout?: int}
  let waitForLoadState: (~t: t, ~state: string=?, ~opts: loadStateOpts=?, unit) => Promise.t<unit>

  type gotoOpts = {waitUntil: string}
  let goto: (t, string, option<gotoOpts>) => Js.Promise.t<unit>
} = {
  type t
  @send external query: (t, string) => Promise.t<Js.Array.t<ElementHandle.t<Node.t>>> = "$$"
  let getLinkElements = t => t->query("a[class=eventCard--link]")
  let getLinks: t => Promise.t<Js.Array.t<Link.t>> = page =>
    page
    ->getLinkElements
    ->Promise.then(els =>
      els
      ->Js.Array2.map(linkEls => linkEls->Link.make)
      ->Promise.all // Array traverse Promise Array<Promise> becomes Promise<Array>
      ->Promise.thenResolve(linkEls => linkEls->Belt.Array.keepMap(id))
    )

  @send
  external isVisible: (t, string) => Promise.t<bool> = "isVisible"
  @send
  external locator: (t, string) => Locator.t = "locator"

  @send
  external close: t => Js.Promise.t<unit> = "close"

  let getMobileModal = page =>
    page
    ->isVisible("data-testid=MobileAppPrompt")
    ->Promise.thenResolve(visible =>
      switch visible {
      | true => Some(page->locator("#modal"))
      | false => None
      }
    )

  let getOneTrustBtn = page =>
    page
    ->isVisible("button#onetrust-accept-btn-handler")
    ->Promise.thenResolve(visible => {
      Js.log("Is visible?")
      Js.log(visible)
      switch visible {
      | true => Some(page->locator("#onetrust-accept-btn-handler"))
      | false => None
      }
    })

  @send external waitForTimeout: (t, int) => Promise.t<unit> = "waitForTimeout"
  @send external waitForSelector: (t, string) => Promise.t<ElementHandle.t<'a>> = "waitForSelector"

  type navOpts = {
    timeout?: int,
    url?: string,
    waitUntil?: string,
  }
  @send
  external waitForNavigation: (~t: t, ~opts: navOpts=?, unit) => Promise.t<unit> =
    "waitForNavigation"

  type loadStateOpts = {timeout?: int}
  @send
  external waitForLoadState: (
    ~t: t,
    ~state: string=?,
    ~opts: loadStateOpts=?,
    unit,
  ) => Promise.t<unit> = "waitForLoadState"

  type gotoOpts = {waitUntil: string}
  @send external goto: (t, string, option<gotoOpts>) => Promise.t<unit> = "goto"
}

module BrowserContext: {
  @genType.import(("playwright", "BrowserContext"))
  type t

  let newPage: t => Js.Promise.t<Page.t>
} = {
  type t
  @send external newPage: t => Js.Promise.t<Page.t> = "newPage"
}
