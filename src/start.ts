import { webkit, devices } from 'playwright'
import type { Page, ElementHandle, Locator } from 'playwright'

const eventSlug = process.argv[2]
console.log(eventSlug)

type Maybe<T> = T | undefined

const Maybe = {
  map: <A, B>(f: (a: A) => B) => (v: Maybe<A>) => v !== undefined ? f(v) : undefined
}

// Event Type
// Event.fromString
const Event = {
  isEvent: (linkText: string) => {
    return linkText && linkText.match(new RegExp(`^/${eventSlug}/.*`))
  }

}

const getHref = async (link: ElementHandle<Node>): Promise<string | null> => await link.getAttribute('href')

const getLinks: (page: Page) => Promise<ElementHandle[]> = async page => await page.$$('a[class=eventCard--link]')

const isGDRPModalVisible: (page: Page) => Promise<boolean> = async page => await page.isVisible('button#onetrust-accept-btn-handler')

const getOneTrustBtn: (page: Page) => Promise<Maybe<Locator>> = async page => {
  if (await page.isVisible('button#onetrust-accept-btn-handler')) { return page.locator('#onetrust-accept-btn-handler') } else return undefined
}

const getMobileModal: (page: Page) => Promise<Maybe<Locator>> = async page => {
  if (await page.isVisible('data-testid=MobileAppPrompt')) { return page.locator('#modal').getByRole('button').first() } else return undefined
}
const getAttendBtn: (page: Page) => Promise<Maybe<Locator>> = async page => {
  if (await page.isVisible('data-testid=attend-irl-btn')) { return page.locator('data-testid=attend-irl-btn') } else return undefined
}

const getEventsOnPage = async (page: Page) => await getLinks(page)
  .then(async links => await Promise.all((links.map(getHref).filter(Boolean))))
  .then(async hrefs => await Promise.resolve(hrefs.filter(Event.isEvent)))

const useAttendBtn = (page: Page) => async (attendBtn: Locator) => {
  await getMobileModal(page)
    // Dismiss mobile modal
    .then(Maybe.map(async btn => { await btn.click() }))
  await page.waitForTimeout(500)

  await attendBtn.click()
  await page.waitForTimeout(500)
  await page.locator('button:has-text("Submit")').click()
  // wait for submit before loading next event
  await page.waitForTimeout(1000)
}

const loadEventOnPage = (page: Page) => async (event: string) => {
  await page.goto('https://www.meetup.com' + event, {
    waitUntil: 'domcontentloaded'
  })

  await page.waitForTimeout(1500)
}

const tryJoinEvent = (page) => (event) => async () => {
  // Run a bunch of side-effects
  console.log(`Attempting to attend ${event}`)

  await loadEventOnPage(page)(event)

  await getAttendBtn(page).then(Maybe.map(useAttendBtn(page)))
}

const runSync = async (jobs) => {
  for (let i = 0; i < jobs.length; i++) {
    await jobs[i]()
  }
}

(async () => {
  const browser = await webkit.launch({ headless: false })
  const context = await browser.newContext(devices['Desktop Safari'])
  const page = await context.newPage()

  await page.goto('https://www.meetup.com/login/')

  await page.type('input[name=email]', 'chris_chen21@yahoo.com')
  await page.type('input[name=current-password]', 'thek2SULL5aub@nul')
  await page.click('button[name=submitButton]')

  await page.waitForTimeout(2000)

  const viewed = []

  while (1) {
    await page.goto(`https://www.meetup.com/${eventSlug}/events`)
    await page.waitForTimeout(1500)

    const events = await getEventsOnPage(page)

    console.log(`Found ${events.length} events`)

    // Close the GDRP modal
    await getOneTrustBtn(page)
      .then(Maybe.map(async (btn) => { await btn.click() }))

    // Run each event synchronously
    const jobs = events.map(tryJoinEvent(page))
    await runSync(jobs)

    await page.waitForTimeout(3 * 60 * 1000)
  }

  // Teardown
  await context.close()
  await browser.close()
})()
