import { webkit, devices } from 'playwright'

const eventSlug = process.argv[2]
console.log(eventSlug);
(async () => {
  const browser = await webkit.launch({ headless: true })
  const context = await browser.newContext(devices['Desktop Safari'])
  const page = await context.newPage()

  await page.goto('https://www.meetup.com/login/')

  await page.type('input[name=email]', 'chris_chen21@yahoo.com')
  await page.type('input[name=current-password]', 'thek2SULL5aub@nul')
  await page.click('button[name=submitButton]')

  await page.waitForTimeout(2000)

  while (1) {
    await page.goto(`https://www.meetup.com/${eventSlug}/events`)
    await page.waitForTimeout(1000)

    const results = await page.$$('a[class=eventCard--link]')

    const links: string[] = []
    results.forEach(async (link) => {
      const linkText = await link.getAttribute('href')
      if (linkText && linkText.match(new RegExp(`^/${eventSlug}/.*`))) { links.push(linkText) }
    })
    await page.waitForTimeout(500)
    console.log(`Found ${links.length} events`)

    let can_attend: boolean
    let modal: boolean

    // Close the GDRP modal
    modal = await page.isVisible('button#onetrust-accept-btn-handler')
    if (modal) await page.locator('#onetrust-accept-btn-handler').click()

    for (let i = 0; i < links.length; i++) {
      console.log(`Attempting to attend ${links[i]}`)
      await page.goto('https://www.meetup.com' + links[i], {
        waitUntil: 'domcontentloaded'
      })

      await page.waitForTimeout(1500)
      can_attend = await page.isVisible('data-testid=attend-irl-btn')
      console.log('Can attend?')
      console.log(can_attend)

      if (can_attend) {
        modal = await page.isVisible('data-testid=MobileAppPrompt')
        if (modal) { await page.locator('#modal').getByRole('button').first().click() }

        await page.locator('data-testid=attend-irl-btn').click()
        await page.waitForTimeout(500)

        await page.locator('button:has-text("Submit")').click()
        await page.waitForTimeout(1000)
      }
    }
    await page.waitForTimeout(3 * 60 * 1000)
  }

  // Teardown
  await context.close()
  await browser.close()
})()
