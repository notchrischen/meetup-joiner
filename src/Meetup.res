/*
let main = Promise.make((resolve, reject) => {
	resolve(. ())
})->
Promise.then(() => {
	Promise.resolve();
}) */

open Playwright

module PromiseExt = {
  type t<'a> = Js.Promise.t<'a>
  let tap = (t: Js.Promise.t<'a>, f) =>
    t->Promise.thenResolve(d => {
      f(d)->ignore
      d
    })

  // Not sure what this combinator is called, but it does lift + tap
  let liftTap = (t: 'a, f: 'a => Js.Promise.t<'b>): Js.Promise.t<'a> => {
    f(t)->Promise.thenResolve(_ => t)
  }
}

module Event: {
  type t = private string
  /* let isEvent: string => bool; */
  let make: (string, string) => option<t>
  let fromLink: (Link.t, string) => option<t>
  let toString: t => string
} = {
  type t = string
  let isEvent = (str, slug) => str->Js.String2.indexOf(slug)

  let make = (str, slug) =>
    switch isEvent(str, slug) {
    | -1 => None
    | _ => Some(str)
    }

  let fromLink = (link: Link.t, slug) => make((link :> string), slug)
  let toString = s => (s :> string)
}

module Events: {
  type t = array<Event.t>
  let fromLinks: (array<Link.t>, string) => t
  let toStrings: t => array<string>
} = {
  type t = array<Event.t>
  let fromLinks = (links: array<Link.t>, slug) =>
    links->Belt.Array.map(link => link->Event.fromLink(slug))->Belt.Array.keepMap(id)

  let toStrings = t => t->Belt.Array.map(Event.toString)
}

/* module EventLink: {
	type t = private Event.t;
	let make: (Link.t, string) => option<t>;
} = {
	type t = Event.t;
	let make = (link: Link.t, slug) => (link :> string)->Event.make(slug)
} */

module LoginPage: {
  type t = private {
    page: Page.t,
    emailInput: Locator.t,
    passwordInput: Locator.t,
    submitBtn: Locator.t,
  }
  let make: Page.t => Js.Promise.t<t>

  type credentials = {
    email: string,
    password: string,
  }
  let login: (t, credentials) => Js.Promise.t<unit>
} = {
  type t = {
    page: Page.t,
    emailInput: Locator.t,
    passwordInput: Locator.t,
    submitBtn: Locator.t,
  }
  let make = page => {
    page
    ->Page.goto("https://www.meetup.com/login/", Some({Page.waitUntil: "networkidle"}))
    ->Promise.thenResolve(_ => {
      page,
      emailInput: page->Page.locator("input[name=email]"),
      passwordInput: page->Page.locator("input[name=current-password]"),
      submitBtn: page->Page.locator("button[name=submitButton]"),
    })
  }

  type credentials = {
    email: string,
    password: string,
  }
  let login = (t, credential) => {
    t.emailInput
    ->Locator.type_(credential.email, None)
    ->Promise.then(_ => t.passwordInput->Locator.type_(credential.password, None))
    ->Promise.then(_ => t.submitBtn->Locator.click(None))
    ->Promise.then(_ => t.page->Page.waitForNavigation(~t=_, ()))
  }
}
module GroupPage: {
  type t = private (Page.t, string)
  @genType
  let make: (Page.t, string) => Js.Promise.t<t>
  @genType
  let getEvents: t => Js.Promise.t<Js.Array.t<Event.t>>
} = {
  type t = (Page.t, string)
  let make = (page, slug) =>
    page
    ->Page.goto("https://www.meetup.com/" ++ slug ++ "/events", Some({waitUntil: "networkidle"}))
    ->Promise.thenResolve(_ => (page, slug))

  let getEvents = ((page, groupName)) =>
    page->Page.getLinks->Promise.thenResolve(links => links->Events.fromLinks(groupName))
}

module EventPage: {
  type t = private {
    page: Page.t,
    attendBtn: Locator.t,
    submitBtn: Locator.t,
    mobileModal: Locator.t,
  }
  let make: (Page.t, string) => Js.Promise.t<t>
  /* let getAttendBtn: t => Js.Promise.t<option<Locator.t>>; */
  let useAttendBtn: t => Js.Promise.t<option<Locator.t>>
} = {
  type t = {
    page: Page.t,
    attendBtn: Locator.t,
    submitBtn: Locator.t,
    mobileModal: Locator.t,
  }

  let getAttendBtn = page => page->Page.locator("data-testid=attend-irl-btn")
  let getSubmitBtn = page => page->Page.locator("button:has-text(\"Submit\")")
  let getMobileModal = page => page->Page.locator("data-testid=MobileAppPrompt")

  let make = (page, event) =>
    page
    /* @TODO: change to after JS is loaded */
    ->Page.goto("https://www.meetup.com" ++ event, Some({waitUntil: "networkidle"}))
    ->Promise.thenResolve(_ => {
      page,
      attendBtn: getAttendBtn(page),
      submitBtn: getSubmitBtn(page),
      mobileModal: getMobileModal(page),
    })

  // The _ use moves the first param so that is the last param, allowing
  // for an eta reduction
  // (_, a, b) becomes (a, b, _)
  // This allows the _ param to be curried
  // Normally functional languages are data-last, but Rescript is data-first, so
  // significant params are placed as the first param instead of last, requiring
  // this syntax
  let useAttendBtn = ({page, attendBtn, submitBtn, mobileModal}) =>
    mobileModal
    ->Locator.toOption
    ->Promise.then(Belt.Option.mapWithDefault(_, Promise.resolve(), modal => modal->Locator.click(None)))
    ->Promise.then(_ =>
      attendBtn
      ->Promise.resolve
      ->Promise.then(Autobot.Locator.tryClick)
      ->Promise.then(
        Belt.Option.mapWithDefault(
          _,

          /* ->(x => { Js.log(x); x;}) */

          // We don't care if typing failed for some fields

          /* ->Autobot.Locator.tryType(_, "yes", None)
           ->Promise.thenResolve(_typeStatus => Some()) */
          Promise.resolve(None),
          _ => {
            page
            ->Playwright.Page.locator("#modal")
            ->Playwright.Locator.locator("input[type=text]", None)
            ->Playwright.Locator.all
            ->Promise.then(
              fields => {
                Js.log("Found text fields")
                Js.log(fields)
                fields
                ->Belt.Array.map(Autobot.Locator.tryType(_, "yes", Some({timeout: 1000})))
                ->Promise.all
                ->Promise.thenResolve(_typingStatuses => Some())
              },
            )
          },
        ),
      )
      ->Promise.then(
        Belt.Option.mapWithDefault(_, Promise.resolve(None), _ => {
          submitBtn->Autobot.Locator.tryClick
        }),
      )
      ->Promise.then(
        Belt.Option.mapWithDefault(_, Promise.resolve(None), _ => {
          Js.log("Waiting for network idle");
          page
          ->Page.waitForNavigation(~t=_, ~opts={waitUntil: "networkidle"}, ())
          ->Promise.thenResolve(_ => Some())
        }),
      )
      ->Promise.then(
        Belt.Option.mapWithDefault(_, Promise.resolve(None), _ => {
          Js.log("Is it visible?");
          Js.log(page->Playwright.Page.isVisible("text=going"))
          Autobot.Locator.get(
            Playwright.Page.locator(page, "text=You're going!")->Locator.first,
            {state: #visible, timeout: 3000},
          )
        }),
      )
    )
}
/* module Links: {
	type t = list<Link.t>;

} = {
	type t = list<Link.t>;
	let fromPage = page => page->Page.getLinkElements;
} */
let tryJoinEvent: (Page.t, Event.t) => Js.Promise.t<option<Event.t>> = (page, event) => {
  Js.log("Trying to join event " ++ event->Event.toString)
  EventPage.make(page, (event :> string))
  ->Promise.then(EventPage.useAttendBtn)
  ->Promise.then(successElement => {page->Page.close->Promise.thenResolve(_ => successElement)})
  ->Promise.thenResolve(successElement => successElement->Belt.Option.map(_ => event))
}

@genType
let tryJoinEvents: (
  BrowserContext.t,
  Js.Array.t<Event.t>,
  Persistence.State.t,
) => array<Js.Promise.t<option<Event.t>>> = (context, events, state) =>
  events
  ->Belt.Array.keep(s => state->Persistence.State.notHave((s :> string)))
  ->Belt.Array.map(event => {
    context->BrowserContext.newPage->Promise.then(page => tryJoinEvent(page, event))
  })

@genType
let doLogin = (page, credentials): Js.Promise.t<unit> =>
  page->LoginPage.make->Promise.then(LoginPage.login(_, credentials))
