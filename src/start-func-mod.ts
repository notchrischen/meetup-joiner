import { webkit, devices } from 'playwright'
import type { Page, ElementHandle, Locator } from 'playwright'
import fs from 'fs/promises'
import * as dotenv from 'dotenv'
import { id } from './start.gen'

dotenv.config()

const eventSlug = process.argv[2]
console.log(eventSlug)

/** Utility Functions **/
// const id = <T>(id: T): T => id;

const runSync = async <T>(jobs: Array<() => Promise<T>>) => {
  for (let i = 0; i < jobs.length; i++) {
    await jobs[i]()
  }
}

/** Domain Primitives **/
enum JSONBrand { _ = '' };
type JSON = string & JSONBrand
type State = Set<string>

const State: {
  fromFile: (file: string) => Promise<Maybe<State>>
  empty: () => State
} = {
  fromFile: async (file: string): Promise<Maybe<State>> => {
    try {
      return new Set(JSON.parse(await fs.readFile(file, { encoding: 'utf8' })) as string[])
    } catch (err) {
      return undefined
    }
  },
  empty: () => new Set()
}

interface PersistedState {
  file: string
  state: State
}
const PersistedState = {
  saveState: async (state: PersistedState): Promise<Maybe<void>> => {
    try {
      await fs.writeFile(state.file, JSON.stringify(state.state)); return
    } catch (err) {
      console.log(err)
      return undefined
    }
  }
}

declare global {
  interface Array<T> {
    tap: (cb: (item: T) => void) => T[]
  }
}

Array.prototype.tap = function <T>(cb: (item: T) => void) {
  return this.map((i: T) => {
    cb(i)
    return i
  })
}

type Maybe<T> = T | undefined

const Maybe = {
  map: <A, B>(f: (a: A) => B) => (v: Maybe<A>): Maybe<B> => v !== undefined ? f(v) : undefined,
  fold: <A, B>(none: B, some: (a: A) => B, maybe: Maybe<A>) => {
    if (maybe === undefined) {
      return none
    } else {
      return some(maybe)
    }
  },
  flatMap: <A, B>(f: (a: A) => Maybe<B>, maybe: Maybe<A>): Maybe<B> => {
    if (maybe !== undefined) {
      return f(maybe)
    }
  },
  bimap: (f1, f2, maybe) => {
    return (maybe === undefined) ? f2() : f1(maybe)
  }
}

// Event Type
// Event.fromString
enum EventBrand { _ = '' };
type Event = string & EventBrand
const Event = {
  isEvent: (slug: string) => (linkText: string): linkText is Event => {
    return linkText.match(new RegExp(`^/${slug}/.*`)) != null
  }

}

const Link = {
  getHref: async (link: ElementHandle<Node>): Promise<string | null> => await link.getAttribute('href')
}

const Page = {
  getLinks: async (page: Page): Promise<ElementHandle[]> => await page.$$('a[class=eventCard--link]'),
  isGDRPModalVisible: async (page: Page): Promise<boolean> => await page.isVisible('button#onetrust-accept-btn-handler'),
  getOneTrustBtn: async (page: Page): Promise<Maybe<Locator>> => {
    if (await page.isVisible('button#onetrust-accept-btn-handler')) { return page.locator('#onetrust-accept-btn-handler') } else return undefined
  },
  getMobileModal: async (page: Page): Promise<Maybe<Locator>> => {
    if (await page.isVisible('data-testid=MobileAppPrompt')) { return page.locator('#modal').getByRole('button').first() } else return undefined
  },
  getEventsOnPage: async (page: Page): Promise<Event[]> => await Page.getLinks(page)
    .then(async links => await Promise.all((links.map(Link.getHref).filter(Boolean))))
    .then(async hrefs => await Promise.resolve(hrefs.filter(Event.isEvent(eventSlug)))),
  tryJoinEvent: (page: Page) => (event) => async () => {
    // Run a bunch of side-effects
    console.log(`Attempting to attend ${event}`)

    const eventPage = await EventPage.make(page)(event)

    await EventPage.getAttendBtn(eventPage).then(Maybe.map(EventPage.useAttendBtn(eventPage)))
  },
  login: async (page: Page, credential: Credential) => {
    const loginPage = await LoginPage.make(page)
    await LoginPage.login(loginPage, credential)
  }
}

enum EventPageBrand { _ = '' };
type EventPage = Page & EventPageBrand
const EventPage = {
  getAttendBtn: async (page: EventPage): Promise<Maybe<Locator>> => {
    if (await page.isVisible('data-testid=attend-irl-btn')) { return page.locator('data-testid=attend-irl-btn') } else return undefined
  },
  useAttendBtn: (page: EventPage) => async (attendBtn: Locator) => {
    await Page.getMobileModal(page)
      // Dismiss mobile modal
      .then(Maybe.map(async btn => { await btn.click() }))
    await page.waitForTimeout(500)

    await attendBtn.click()
    await page.waitForTimeout(500)
    await page.locator('button:has-text("Submit")').click()
    // wait for submit before loading next event
    await page.waitForTimeout(1000)
  },
  make: (page: Page) => async (event: string): Promise<EventPage> => {
    await page.goto('https://www.meetup.com' + event, {
      waitUntil: 'domcontentloaded'
    })

    await page.waitForTimeout(1500)
    return page as EventPage
  }
}

interface Credential {
  email: string
  password: string
}

const Credential = {
  make: (email: string) => (password: string): Credential => ({
    email,
    password
  })
}

enum LoginPageBrand { _ = '' };
type LoginPage = Page & LoginPageBrand
const LoginPage = {
  login: async (page: LoginPage, credential: Credential) => {
    await page.type('input[name=email]', credential.email)
    await page.type('input[name=current-password]', credential.password)
    await page.click('button[name=submitButton]')

    await page.waitForTimeout(2000)
  },
  make: async (page: Page) => {
    await page.goto('https://www.meetup.com/login/'); return page as LoginPage
  }
};

(async () => {
  const browser = await webkit.launch({ headless: false })
  const context = await browser.newContext(devices['Desktop Safari'])
  const page = await context.newPage()

  const credential = Maybe.flatMap(
    email => Maybe.flatMap(
      password => Credential.make(email)(password),
      process.env.PASSWORD
    ),
    process.env.EMAIL)

  await Maybe.bimap(
    async credential => { await Page.login(page, credential) },
    () => { throw Error('.env file required.') },
    credential)

  const state = await State.fromFile('./events.state')
  const joined: State = Maybe.fold(State.empty(), id, state)

  while (1) {
    await page.goto(`https://www.meetup.com/${eventSlug}/events`)
    await page.waitForTimeout(1500)

    const events = await Page.getEventsOnPage(page)

    console.log(`Found ${events.length} events`)

    // Close the GDRP modal
    await Page.getOneTrustBtn(page)
      .then(Maybe.map(async (btn) => { await btn.click() }))

    const actions = events.filter(event => !joined.has(event))
      .tap(event => joined.add(event))
      .map(Page.tryJoinEvent(page))

    console.log(`\t${actions.length} new event(s)`)

    // Run each join action synchronously
    await runSync(actions)

    PersistedState.saveState({ file: './events.state', state: joined })

    await page.waitForTimeout(2 * 60 * 1000)
  }

  // Teardown
  await context.close()
  await browser.close()
})()
