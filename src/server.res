let events = [("WoSP Badminton Club", "wosp-badminton-club"), ("Tokyo Badcox - International Badminton Club", "tokyo-badcox-international-badminton-club")]
let eventsHash = Belt.HashMap.String.fromArray(events)

module OptionExt = {
  open Belt.Option
  type t<'a> = option<'a>
  let toResult = (opt: t<'a>, errMsg) => opt->mapWithDefault(Error(errMsg), d => Ok(d))
}

module DotEnv = {
  type t
  @module("dotenv") external config: unit => unit = "config"
}

module Helpers = {
  type playwright
  @module("./helpers.js") external joinEvents: string => Js.Promise.t<playwright> = "joinEvents"
  @module("./helpers.js")
  external cleanupPlaywright: playwright => Js.Promise.t<unit> = "cleanupPlaywright"
}

module PostmarkHook = {
  type t = {
    name: string,
    messageStream: string,
    from: string,
    subject: string,
    date: Js.Date.t,
  }
  let make = (name, messageStream, from, subject, date) => {
    name,
    messageStream,
    from,
    subject,
    date,
  }

  let findEvent = (data: t, events): option<string> =>
    events
    ->Belt.Array.getBy(((name, _)) => data.name->Js.String2.includes(name))
    ->Belt.Option.map(((_, slug)) => slug)
}

module PostmarkHookDto = {
  type t = {
    "FromName": string,
    "MessageStream": string,
    "From": string,
    "Subject": string,
    "Date": string,
  }

  let fromJs = js => {
    open JsonCombinators.Json.Decode
    js->decode(
      object(field =>
        {
          "FromName": field.required(. "FromName", string),
          "MessageStream": field.required(. "MessageStream", string),
          "From": field.required(. "From", string),
          "Subject": field.required(. "Subject", string),
          "Date": field.required(. "Date", date),
        }
      ),
    )
  }

  let toDomain = (js: Js.Json.t) =>
    js
    ->fromJs
    ->Belt.Result.map(t =>
      PostmarkHook.make(t["FromName"], t["MessageStream"], t["From"], t["Subject"], t["Date"])
    )
}

module Fastify = {
  type t

  type opts = {logger: bool}
  type listenOpts = {port: int}

  module Req = {
    type t = {body: Js.Json.t}
  }

  @module("fastify") external make: opts => t = "default"

  @send external listen: (t, listenOpts) => Promise.t<unit> = "listen"
  @send external get: (t, string, ('c, 'd) => option<Js.Json.t>) => unit = "get"
  @send external post: (t, string, (Req.t, 'd) => option<Js.Json.t>) => unit = "post"

  module Reply = {
    type t
    @send external send: (t, Js.Json.t) => t = "send"
  }
}

DotEnv.config()

let fastify = Fastify.make({logger: true})

fastify->Fastify.post("/", (req, _reply) => {
  open Belt.Result
  open JsonCombinators.Json.Encode
  /* _reply->Fastify.Reply.send(object([("hello", string("world"))]))->ignore; */
  /* None; */
  req.body
  ->PostmarkHookDto.toDomain
  ->flatMap(dto => dto->PostmarkHook.findEvent(events)->OptionExt.toResult("Not found"))
  ->map(slug => slug->Helpers.joinEvents->Promise.then(Helpers.cleanupPlaywright))
  ->ignore

  Some(object([("ok", bool(true))]))
})

let start = () => {
  open Fastify
  fastify->listen({port: 3000})
}

start()->ignore
