/* TypeScript file generated from Meetup.res by genType. */
/* eslint-disable import/first */


// @ts-ignore: Implicit any on import
import * as Curry__Es6Import from 'rescript/lib/es6/curry.js';
const Curry: any = Curry__Es6Import;

// @ts-ignore: Implicit any on import
import * as MeetupBS__Es6Import from './Meetup.mjs';
const MeetupBS: any = MeetupBS__Es6Import;

import type {BrowserContext_t as Playwright_BrowserContext_t} from './Playwright.gen';

import type {Page_t as Playwright_Page_t} from './Playwright.gen';

import type {State_t as Persistence_State_t} from './Persistence.gen';

// tslint:disable-next-line:interface-over-type-literal
export type Event_t = string;

// tslint:disable-next-line:interface-over-type-literal
export type LoginPage_credentials = { readonly email: string; readonly password: string };

// tslint:disable-next-line:interface-over-type-literal
export type GroupPage_t = [Playwright_Page_t, string];

export const GroupPage_make: (_1:Playwright_Page_t, _2:string) => Promise<GroupPage_t> = function (Arg1: any, Arg2: any) {
  const result = Curry._2(MeetupBS.GroupPage.make, Arg1, Arg2);
  return result
};

export const GroupPage_getEvents: (_1:GroupPage_t) => Promise<Event_t[]> = MeetupBS.GroupPage.getEvents;

export const tryJoinEvents: (context:Playwright_BrowserContext_t, events:Event_t[], state:Persistence_State_t) => Array<Promise<(null | undefined | Event_t)>> = function (Arg1: any, Arg2: any, Arg3: any) {
  const result = Curry._3(MeetupBS.tryJoinEvents, Arg1, Arg2, Arg3);
  return result
};

export const doLogin: (page:Playwright_Page_t, credentials:LoginPage_credentials) => Promise<void> = function (Arg1: any, Arg2: any) {
  const result = Curry._2(MeetupBS.doLogin, Arg1, Arg2);
  return result
};
