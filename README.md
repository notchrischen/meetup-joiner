# Meetup Joiner

## Getting started

Requires yarn and nodejs.

Rename .env.sample to .env, and add your Meetup credentials.

```brew install node```
Install yarn via instructions: https://yarnpkg.com/getting-started/install

Run ```yarn install``` in project directory.

Run ```yarn build``` in project directory to compile JS files.

Run ```yarn start``` which launches an HTTP server listening on port 3000.

All POST requests hitting the server endpoint will trigger the workflow to check
for events to join on Meetup.

***

# Project Structure

This project is implemented in 4 different ways in 4 separate rewrites: 1)
imperative, 2) functional with mostly referentially transparent functions, 3)
functional with functions moved into categories, monads, and combinators, and 4) Rescript–same as 3 but in an actual functional language with first class currying support and a sound type system

## Imperative - src/start-imperative.ts
Logic is strictly non-reusable unless you copy and paste literal blocks of code. Repeated logic must be completed refactored if we want to change fundamental behavior.

## Basic Functional - src/start.ts
Reusable logic is moved into mostly referentially transparent (pure) functions,
but there is no concept of higher level categories. All functions live at the
top level. A Maybe monad is technically used but is implemented in categorical
way with all Maybe functions (just the one map function) placed inside that type.

## Functional with Categories - src/start-func-mod.ts
The rest of the functions are placed into type modules like how the Maybe monad
is implemented. Functions are mostly unary and operate on algebraic data types,
allowing for immutable pipelines of data operated on by pure functions.

## Rescript - src/start.res and src/start-res.ts
The above is re-implemented in Rescript (for the most part) which demonstrates
the advantages of a language with first class sum types, new abstract types,
smart type constructors, currying, and a sound type system.

The src/start-res.ts file is the entrypoint implemented in Typescript, and shows
how easy interop between Rescript and Typescript is.

## Roadmap
- Add a handler for parsing Meetup notification emails
- Simple notification hook endpoint of some kind to invoke handler

## Contributing
Send diffs through LINE.

## Authors and acknowledgment
Chris Chen

## License
Do not redistribute. For private personal use only.

## Project status
Active
